angular
  .module('app')
  .controller('MeasurementsController', ['$scope', 'Measurement', function($scope,
    Measurement) {
    $scope.measurements = Measurement.find({
      filter: {
        include: [
          'branch'
        ]
      }
    });
  }]);