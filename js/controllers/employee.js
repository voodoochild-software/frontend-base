angular
  .module('app')
  .controller('EmployeesController', ['$scope', 'Employee', function($scope,
    Employee) {
    $scope.employees = Employee.find({});
  }]);